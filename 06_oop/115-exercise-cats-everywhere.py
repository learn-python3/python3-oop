# Given the below class:
class Cat:
    species = 'mammal'

    def __init__(self, name, age):
        self.name = name
        self.age = age


# 1 Instantiate the Cat object with 3 cats
cat1 = Cat('Milli', 17)
cat2 = Cat('Filou', 22)
cat3 = Cat('Casimir', 19)


# 2 Create a function that finds the oldest cat
def find_oldest_cat(cats=list()):
    if len(cats) == 0:
        return 'The cat list is empty!'

    sorted_cats = sorted(cats, key=lambda x: x.age, reverse=True)
    return sorted_cats[0]


# 3 Print out: "The oldest cat is x years old.". x will be the oldest cat age by using the function in #2
oldest_cat = find_oldest_cat([cat1, cat2, cat3])
print(f'The oldest cat is {oldest_cat.age} years old.')
